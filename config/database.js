require('dotenv').config();

/**
 * Database Configuration
 *
 * Configuration of local database
 *  mongoose.connect('mongodb://localhost/databaseName');
 *
 */

const MONGODB = process.env.NODE_APP_MONGODB_BASE;

let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(`${MONGODB}`, {useMongoClient: true});

/**
 * Function to print in console if the database connection was a success
 */
mongoose.connection.on('connected', function () {
    console.log('Connected to MongoDB');
});

/**
 * Avoiding close the application if not get the connection
 */
mongoose.connection.on('error', function (error) {
    console.log('DB error: ' + error);
});

/**
 * Avoiding close the application if lost connection
 */
mongoose.connection.on('disconnected', function () {
    console.log('Disconnected MongoDB');
});

/**
 * Accessing the process to ensure close the database connection when the application finish
 */
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('db close');
        process.exit(0);
    });
});
