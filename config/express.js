const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const consign = require("consign");
const path = require("path");
const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));
server.use(cookieParser());
server.set("secret", "opensecret");
server.use(
  cors({
    origin: "*",

    allowedHeaders: [
      "Content-Type",
      "Authorization",
      "Origin",
      "X-Requested-With",
      " X-Total-Count"
    ],
    exposedHeaders: ["Content-Type", "X-Total-Count"],
    allowedMethods: ["GET", "PUT", "POST", "DELETE", "OPTIONS"]
  })
);

server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Expose-Headers", "X-Total-Count");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Total-Count"
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  next();
});

consign({ cwd: path.join(process.cwd(), "src") })
  .include("models")
  .then("controllers")
  .then(path.join("routes", "auth.js"))
  .then("routes")
  .into(server);

module.exports = server;
