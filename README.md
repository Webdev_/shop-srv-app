﻿## Node API

## Collections

#### Schema User (users)

| Name | Description | Type |
| ------ | ------ | ------ |
| password | user password (hash) | String |
| email | user mail  | String |
| airtableUserId | airtable id  | String |

## API Directories

- Routes ```./src/routes```
- Models ```./src/models```
- Controllers ```./src/controllers```
- Services ```./src/services```
- Configurations of Express ```./config/express.js```
- Database configurations ```./config/database.js```
- Server configurations ```./server.js```

## Configuring the API locally

- Download or clone the project access the project folder with the terminal and execute the CLI <code>npm install</code>
- Config your database in ```./congig/database.js``` change ```mongoose.connect('mongodb://localhost/yourDatabaseName');```
- Run the server in development mode <code>npm run dev</code>
- <code>Ctrl + c</code> to exit of logs and run <code>pm2 kill</code> to kill all process of pm2
- Access in your browser <a href="http://localhost:3000/projects">http://localhost:3000/login</a>