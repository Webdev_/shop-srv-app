const http = require('http');
const server = require('./config/express');
require('./config/database');
require('dotenv').config();

const PORT =  process.env.NODE_APP_MONGODB_BASE_PORT;


server.use(function(req,res,next){
	res.header("Access-Control-Allow-Origin","*");
	res.header("Access-Control-Expose-Headers","X-Total-Count");
	res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Total-Count");
	res.header("Access-Control-Allow-Methods","GET,PUT,POST,DELETE,OPTIONS");
	next();
});

server.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});
