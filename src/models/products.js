const mongoose = require('mongoose');


const schema = mongoose.Schema({


    name:{
        type:String,
        require: true
    },
    amount:{
        type: String,
        require: false
    },
    purchaseId:{
        type: String,
        require: true
    }


})

mongoose.model('products',schema);