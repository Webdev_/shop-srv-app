module.exports = src => {
  const controller = src.controllers.shopListActions;

  src.get("/purchases", controller.showAllUserLists);
  src.get("/products", controller.showAllProductInList);

  src.post("/purchases/new", controller.addNewList);
  src.post("/products/new", controller.addProductInList);

  src.put("/purchases/update", controller.updateList);
  src.put("/products/update", controller.updateProduct);

  src.delete("/purchases/remove", controller.removeList);
  src.delete("/products/remove", controller.removeProduct);
};
