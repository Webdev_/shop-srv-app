module.exports = (src) => {

    const controller = src.controllers.auth;
    const controllerUser = src.controllers.user;

    src.post('/profile/login', controller.login);
    src.post('/profile/register',controllerUser.create);  
   

    /**
     * route to authenticate the user using Json Web Token
     */
    src.use('/*', controller.verifyToken);
};
