const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const saltRounds = 15;
const jwt = require("jsonwebtoken");
const model = mongoose.model("User");
require("dotenv").config();

module.exports = UserCtrl => {
  let controller = {};
  const SECRET = UserCtrl.get("secret");

  controller.list = async (req, res) => {
    try {
      let users = await model.find({});
      res.status(200).json(users);
    } catch (e) {
      res.status(500).json(e);
    }
  };

  controller.create = async (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
      req.body.password = hash;

      try {
        let userObject = req.body;

        let localUser = {};

        let isUserExist = await model.find({
          email: userObject.email
        });

        if (isUserExist.length) {
          const error = new Error("User exists");
          error.code = "User exists";
          error.status = 409;
          throw error;
        }

        console.log(userObject);
        if (!userObject.email) {
          throw { code: "email required", message: "Email required" };
        }

        localUser.email = userObject.email;

        localUser.name = userObject.name;

        localUser.password = userObject.password;

        let user = await model.create(localUser);

        res.status(200).json({ code: "Register Done!" });
      } catch (e) {
        res.status(e.status).json(e);
      }
    });
  };

  controller.update = async (req, res) => {
    bcrypt.hash(req.body.password, saltRounds, async (err, hash) => {
      req.body.password = hash;

      try {
        let user = await model.findById(req.user.user_id);

        //let user = await model.findByIdAndUpdate(req.user.user_id, req.body);
        res.status(200).json(update);
      } catch (e) {
        res.status(404).json(e);
      }
    });
  };
  return controller;
};
