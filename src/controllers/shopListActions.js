const mongoose = require("mongoose");
const modelPurchases = mongoose.model("purchases");
const modelProducts = mongoose.model("products");
const modelUser = mongoose.model("User");

module.exports = shopListActions => {
  let controller = {};

  controller.addNewList = async (req, res) => {
    try {
      let inputObject = req.body;

      let objectToDB = {};

      objectToDB.name = inputObject.name;
      objectToDB.userId = req.user.user_id;

      let newList = await modelPurchases.create(objectToDB);

      res.status(200).json({
        list: { name: newList.name, id: newList._id }
      });
    } catch (e) {
      res.status(401).json(e);
    }
  };

  controller.updateList = async (req, res) => {
    try {
      let inputObject = req.body;

      let update = await modelPurchases.findByIdAndUpdate(inputObject.id, {
        name: inputObject.name
      });

      let updatedList = await modelPurchases.findById(inputObject.id);

      res.status(200).json({
        newList: { name: updatedList.name, id: inputObject.id }
      });
    } catch (e) {
      res.status(404).json(e);
    }
  };

  controller.showAllUserLists = async (req, res) => {
    try {
      let allLists = [];

      let allListsObject = await modelPurchases.find({
        userId: req.user.user_id
      });

      if (!allListsObject.length) {
        res.status(200).json({ code: "Хуй блядь, нет списков!" });
      }

      if (allListsObject.length >= 2) {
        for (let i = 0; i < allListsObject.length; i++) {
          let corectedUbject = {};

          corectedUbject.name = allListsObject[i].name;
          corectedUbject.id = allListsObject[i]._id;
          allLists[i] = corectedUbject;
        }

        res.status(200).json({ purchases: allLists });
      } else {
        res.status(200).json({ purchases: allListsObject });
      }
    } catch (e) {
      res.status(404).json(e);
    }
  };

  controller.removeList = async (req, res) => {
    try {
      let id = req.body.id;

      let removedList = await modelPurchases.findByIdAndRemove(id);

      res.status(200).json({ code: `Done, list ${removedList.name} removed!` });
    } catch (e) {
      res.status(500).json(e);
    }
  };

  controller.addProductInList = async (req, res) => {
    try {
      let products = req.body;

      let newItem = await modelProducts.create(products);

      let resonseObject = {
        name: newItem.name,
        purchaseId: newItem.purchaseId,
        amount: newItem.amount,
        id: newItem._id
      };

      res.status(200).json({ product: resonseObject });
    } catch (e) {
      res.status(500).json(e);
    }
  };

  controller.updateProduct = async (req, res) => {
    try {
      let inputObject = req.body;

      let update = await findByIdAndUpdate(inputObject.id, {
        name: inputObject.name,
        amount: inputObject.amount
      });

      let updatedList = await modelPurchases.findById(inputObject.id);

      res.status(200).json({ product: updatedList });
    } catch (e) {
      res.status(404).json(e);
    }
  };

  controller.showAllProductInList = async (req, res) => {
    try {
      let allProducts = await modelProducts.find();

      console.log("allProduct: ", allProducts);

      res.status(200).json({ products: allProducts });
    } catch (e) {
      res.status(404).json(e);
    }
  };

  controller.removeProduct = async (req, res) => {
    try {
      let id = req.body.id;

      let removeItem = await findByIdAndRemove(id);

      res
        .status(200)
        .json({ code: `Done, product ${removeItem.name} removed!` });
    } catch (e) {
      res.status(500).json(e);
    }
  };

  return controller;
};
