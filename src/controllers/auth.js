const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const model = mongoose.model("User");

module.exports = app => {
  let controller = {};
  const SECRET = app.get("secret");

  controller.login = async (req, res) => {
    try {
      let user = await model.findOne({ email: req.body.email });

      console.log("\nUser: " + user);

      bcrypt.compare(req.body.password, user.password, (error, success) => {
        if (success) {
          const token = jwt.sign({ user_id: user._id }, SECRET);

          return res
            .status(200)
            .json({ _id: user._id, email: user.email, token: token });
        } else {
          res.status(401).send({ error: error, message: "Password mismatch" });
        }
      });
    } catch (e) {
      console.log({ e });
      res
        .status(401)
        .send({ error: e, message: "Error, user does not exists" });
    }
  };

  controller.verifyToken = (req, res, next) => {
    const TOKEN = req.get("Authorization");

    if (!TOKEN) {
      res.status(401).send("Token is required");
      return false;
    }

    jwt.verify(TOKEN, SECRET, (error, decoded) => {
      if (error) {
        res.status(401).send({ error: error, message: "Invalid Token" });
      } else {
        req.user = decoded;
        next();
      }
    });
  };

  return controller;
};
